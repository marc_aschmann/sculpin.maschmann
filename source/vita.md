---
layout: default
title: vita
description: main page, cv and description
---
## profile

<div class="row">
    <div class="col-md-6">
        <h4><i class="fa fa-cog font-md"></i> technical</h4>
        <ul class="fa-ul">
            <li><i class="fa-li fa fa-check-square"></i>php-based e-commerce systems</li>
            <li><i class="fa-li fa fa-check-square"></i>Symfony2 enthusiast</li>
            <li><i class="fa-li fa fa-check-square"></i>Silverstripe CMS module contributor</li>
            <li><i class="fa-li fa fa-check-square"></i>webstandards enthusiast</li>
        </ul>
    </div>
    <div class="col-md-6">
        <h4><i class="fa fa-music font-md"></i> music</h4>
        <ul class="fa-ul">
            <li><i class="fa-li fa fa-check-square"></i>bass player</li>
            <li><i class="fa-li fa fa-check-square"></i>a little bit of guitar &amp; drums</li>
            <li><i class="fa-li fa fa-check-square"></i>active live-musician</li>
        </ul>
    </div>
</div>

<hr>

## technologies

<div class="row">
    <div class="col-md-6">
        <h4><i class="fa fa-cog font-md"></i> development</h4>
        <ul class="fa-ul">
            <li><i class="fa-li fa fa-check-square"></i>PHP/JavaScript/Python/C++</li>
            <li><i class="fa-li fa fa-check-square"></i>MySQL/MongoDB/MSSQL/Redis/Elasticsearch</li>
            <li><i class="fa-li fa fa-check-square"></i>RabbitMQ</li>
            <li><i class="fa-li fa fa-check-square"></i>HTML5/CSS(3)</li>
            <li><i class="fa-li fa fa-check-square"></i>bash</li>
        </ul>
    </div>
    <div class="col-md-6">
        <h4><i class="fa fa-cog font-md"></i> software/frameworks</h4>
        <ul class="fa-ul">
            <li><i class="fa-li fa fa-check-square"></i><a href="http://symfony.com" title="symfony framework">Symfony2</a> <i class="fa fa-link font-md"></i></li>
            <li><i class="fa-li fa fa-check-square"></i><a href="http://framework.zend.com" title="zend framework">ZendFramework 1 & 2</a> <i class="fa fa-link font-md"></i></li>
            <li><i class="fa-li fa fa-check-square"></i>Vim, <a href="http://www.jetbrains.com/phpstorm/" title="phpstorm ide">PhpStorm</a> <i class="fa fa-link font-md"></i>, <a href="http://www.eclipse.org" title="eclipse ide">Eclipse</a> <i class="fa fa-link font-md"></i></li>
            <li><i class="fa-li fa fa-check-square"></i>Debian/Ubuntu/Mint</li>
        </ul>
    </div>
</div>

<hr>

## <i class="fa fa-wrench font-md"></i> work

### 2014 - now
* senior software engineer/ lead developer
* [Motor Presse Stuttgart](http://www.motorpresse.de/ "Motor Presse Stuttgart") <i class="fa fa-link font-md"></i>

### 2012 - 2014
* senior software engineer
* [internetstores GmbH](http://www.internetstores.de "internetstores GmbH") <i class="fa fa-link font-md"></i>

### 2008-2012
* software engineer
* [digital media center GmbH](http://www.dmc.de "digital media center GmbH") <i class="fa fa-link font-md"></i>

### 2007-2008
* software engineer webEdition
* [IMCOR GmbH](http://www.imcor.de "IMCOR GmbH") <i class="fa fa-link font-md"></i>

### 2007
* external software engineer/administrator
* [Daimler AG](http://www.daimler.com/ "Daimler AG") <i class="fa fa-link font-md"></i>

### 2006-2007
* software engineer/consultant
* [solics GmbH](http://www.solics.de "solics GmbH") <i class="fa fa-link font-md"></i>

### 2006
* IT specialist (IHK)
