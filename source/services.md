---
layout: default
title: services
description: services
---

## <i class="fa fa-cog font-md"></i> services

*****
You can book me any time for the following topics:

<i class="fa fa-check-square-o font-md"></i> Software architeture (php)<br />
<i class="fa fa-check-square-o font-md"></i> Trainings/Workshops (symfony2, design patterns, ...)<br />
<i class="fa fa-check-square-o font-md"></i> Programming/Project support (symfony2, php)<br />

Just <i class="fa fa-envelope font-md"></i> <a href="mailto:&#109;&#097;&#114;&#099;&#064;&#097;&#115;&#099;&#104;&#109;&#097;&#110;&#110;&#046;&#111;&#114;&#103;">contact me</a>.
*****