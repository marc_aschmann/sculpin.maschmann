---
layout: default
title: projects
description: projects
---

## <i class="fa fa-cog font-md"></i> projects

*****

### <i class="fa fa-folder-open font-md"></i> phpflo

* Flow based programming approach in php.
* Refactored/enhanced base library.
* Added parser (phpflo-fbp) for domain specific language.
* Ready for use with symfony or as a standalone ETL tool.
* [phpflo on GitHub](https://github.com/bergie/phpflo "phpflo on GitHub") <i class="fa fa-link font-md"></i>
* [phpflo projects on GitHub](https://github.com/phpflo "phpflo projects on GitHub") <i class="fa fa-link font-md"></i>

*****

### <i class="fa fa-folder-open font-md"></i> php-ansible

* Feature complete library, based on commandline ansible provision tool and Symfony's ProcessComponent.
* [php-ansible on GitHub](https://github.com/maschmann/php-ansible "php-ansible on GitHub") <i class="fa fa-link font-md"></i>

*****

### <i class="fa fa-folder-open font-md"></i> ansible-devbox

* My personal development environment, built on Virtualbox with [vagrant](http://getvagrant.com) and [ansible](http://www.ansible.com).
* Uses Ubuntu 15.04, nginx, php-fpm, MariaDB and a lot of love.
* [devbox-ansible on GitHub](https://github.com/maschmann/devbox-ansible "devbox-ansible on GitHub") <i class="fa fa-link font-md"></i>

*****

### <i class="fa fa-folder-open font-md"></i> ServerGrove Symfony2 deployment

* Helping to develop/maintain this ansible-galaxy package for ServerGrove.
* Capistrano-style deployment for Symfony2 applications, allows for a lot of configuration.
* [Symfony2 deploy on GitHub](https://github.com/servergrove/ansible-symfony2 "Symfony2 deploy on GitHub") <i class="fa fa-link font-md"></i>

*****

### <i class="fa fa-folder-open font-md"></i> TranslationLoaderBundle (Symfony2)

* Symfony framework bundle to load translations from database, also has some import/export commands to help with migration.
* [TranslationLoaderBundle on GitHub](https://github.com/maschmann/TranslationLoaderBundle "TranslationLoaderBundle on GitHub") <i class="fa fa-link font-md"></i>

*****

### <i class="fa fa-folder-open font-md"></i> MarkdownContentBundle (Symfony2)

* This bundle allows MarkdownContent files as html static pages in sf2.
* [MarkdownContentBundle on GitHub](https://github.com/maschmann/MarkdownContentBundle "MarkdownContentBundle on GitHub") <i class="fa fa-link font-md"></i>

*****

### <i class="fa fa-folder-open font-md"></i> php-utilities

* library providing data storage object(s), configuration objects and timers
* the library is extensively tested
* You can find [php-utilities on GitHub](https://github.com/maschmann/php-utilities "php-utilities on GitHub") <i class="fa fa-link font-md"></i>

*****
