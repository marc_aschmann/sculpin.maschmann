---
title: XCache made me lose my nerves!
tags:
    - php
    - symfony2
    - software architecture
    - caching
    - XCache
categories:
    - php
    - symfony2
    - software architecture
    - caching
    - XCache
---
Another year, another trial...
But let's start from the beginning, shall we?

## Introduction
Since my last post a lot has changed in way of development environment. This is due to my new job @ Motorpresse Stuttgart where I started as lead developer in november, last year.
Since the server provider is quite fond of Ubuntu and prefers the "good old apache" stuff, I had to create new virtual machines, ansible roles and tons of configuration to match the server's state. Most annoying: Apache2 (2.2 and 2.4) with either mod_php5 and php-fpm. So I decidede to set up the upcoming refactoring/redesign of serveral projects with a symfony2 skeleton and API application. One more constraint for the environment is XCache, which I know to be slower than APC and less reliable. But whatever floats the admin's boat... 

## The application
Main goal of the new application design is to decouple an old CMS with messy data structure and even worse templating from a - yet to be finished - modern symfony2/gulp/sass driven frontend. The data access is asynchronous with triggered CRUD events, ending in a message queue where they are used to manage the separate frontend data structure.
This results in me having two instances of symfony2 running on one machine.

## XCache introduces itself
By default the new Zend Opcode Cache module is disabled in php.ini, so no crossfire between XCache and php. So I thought.
Having the frontend runnig, I started work on the API and when I tried to call both frontends at once, a "AppKernel cannot be redeclared" error occured. Ok, strange... I only have one AppKernel per project and since the classes are loaded via absolute path...

## Ways to solve the problem
An hour later, I had divided my two apps into two php-fpm pools before realizing that all caching takes place in the master process. At that point I deactivated XCache -> everyting works fine.
OK, XCache variable cache on... Everything works fine. Conclusion: I need another opcode cache.
XCache is able to run with disabled opcode cache - thank god - and the Zend opcode cache is not crashing when being enabled at the same time.

The whole process of figuring out why my app crashed in the first place and therefore fixing it, took me the better part of one day. Great stuff.
Really pissed by XCache.