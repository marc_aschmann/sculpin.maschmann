---
title: New page generator
categories:
    - feature

---

Gladly I can announce to have switched from [pico](http://pico.dev7studios.com "pico") to [sculpin](http://sculpin.io "sculpin statig page generator"). Sculpin is a static page generator, using markdown like pico and basically working like Jekyll :-)
Equipped like that, I decided to add a blog once more. This time mostly about coding...

Hopefully this time I'll be able to work on posts more frequently than before.