---
title: How to Bundle!
tags:
    - php
    - symfony2
    - software architecture
categories:
    - php
    - symfony2
    - software architecture
---
This post ist partly a recap of the symfony life 2014 in Berlin where [Toni (@havvg)](https://github.com/havvg) and I did a "Lightning-Rant" on this topic.
For some time now, we've been sporadically discussing the placement of models in the "VC-style-framework" symfony2. SFUGSTR did a whole meeting about that [back in 2013](http://www.meetup.com/sfugstr/events/105138812/) with great and controversial discussion :-)
And since the new "[Best Practices](http://symfony.com/doc/current/best_practices/index.html)" book for development with symfony [has emerged](http://symfony.com/blog/introducing-the-official-symfony-best-practices), I think it's time to write a little something myself.

## Basics
As of now we've witnessed a lot of developers in projects using bundles to structure their applications, which - in itself - is no bad thing per se. Especially when starting to develop with symfony2 for the first time, it is not that easy to fully grasp the abstractness of the architecture and structure code accordingly.
The basic idea of a bundle is having a container to put alle framework-related or -coupled stuff in a dedicated place where it can be found and interpreted. That's it! **Framework stuff**!
A lot of people start packing "utilities", models, ... into a "CoreBundle" or "AppBundle" and that's not right (from an architectural point of view). To solve this misery there are a few approaches and I am going to discuss two of them.

## Scenario
I assume following scenario, which I think is most common:

* we have an application we know to most likely stay with symfony2 as framework
* we are likely to have a lot of business logic
* the application uses a database and therefore entities and repositories

## The "one bundle to rule them all" approach
This is my preferred approach ;-)
We're using exactly one bundle, no vendor namespace - I love calling it the "CoreBundle" - and place a second folder into the `<project>src/` folder, called "Core" or whatever you want to name your business logic container. Within this container, I usually place _every part of my business logic_ I do not get as an external library via packagist. To access this stuff, I create service definitions within the CoreBundle. Also I tend to place all controllers within the bundle, since they are also tightly coupled to the framework.

**Structure:**
<pre>
project_root
 |- app
 |  |- ...
 |
 |- src
 |  |- CoreBundle
 |  |- Core
 |    |- subnamespace1
 |    |- subnamespace2
 |    |- ...
 | 
 ...
</pre>

In my opinion this looks quite concise and also I love the extra part of the namespace, containing all my business logic (Core).

**Benefits:**

* The overall structuring directory-wise is easy to grasp for people working with sf2
* No changes concerning class-loading is needed, everything works out of the box

## The "no bundle" approach
[Toni (@havvg)](https://github.com/havvg) is really fond of getting rid of all subfolders and bundles, so he suggests to pack all code _flat_ into the `src/` directory. You can find a sample project skeleton on [GitHub](https://github.com/havvg/symfony-sandbox).

**Structure:**
<pre>
project_root
 |- app
 |  |- ...
 |
 |- src
 |  |- Controller
 |  |- Entity
 |  |- Repository
 |  |- ....
 | 
 ...
</pre>

As you can see, this flattens the project structure in an immense way and also allows for nice DDD. Downside is: You have to overwrite e.g. the ApplicationKernel to be able to find Controllers and locate all the other stuff. For the people asking themselves "where are the templates and other files?!": They need to be placed in `app/Resources/`.

## What does this mean for my OSS bundles?
I know, there are loads of bundles around in symfony-world. And lots of them have business logic within their directory structure.
If you want to de-clutter this mess, try following approach: Pack all the relevant business-logic into a framework-decoupled package (library!), promote it on packagist and **then** write a bundle to make the library easily accessible via frameworks like symfony. Read [this article](https://medium.com/@christophewillemsen/stop-making-bundles-think-bundles-deadd27b88c0) for a nice approach to make your bundle framework-interoperable!

More to read on this topic:

* [Stop making bundles, think bundles!](https://medium.com/@christophewillemsen/stop-making-bundles-think-bundles-deadd27b88c0)
* [The naked bundle](http://de.slideshare.net/matthiasnoback/high-quality-symfony-bundles-tutorial-dutch-php-conference-2014)
* [symfony2 bundle, standalone](http://gnugat.github.io/2014/10/29/sf2-bundle-standalone.html)

