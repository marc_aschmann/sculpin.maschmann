---
title: Some thoughts on technical debt and efficiency
tags:
    - php
    - architecture
    - work
categories:
    - php
    - architecture
    - work
---

For the past years I've been spending a lot of thought on how to improve things. As a softwar developer you might know this: You see something, a workflow or just a tiny task and you already start thinking "If I change this..." and a short while later the thing you just looked at is e.g. better, more efficient or just plain faster or easier to do.

But how to get there?

Let's go back a few years to a point where I first realized this.
I was in-between schools and had nothing to do, so I started working at my father's company. My father is a craftsman, insulating roofs (flat ones, especially) and does so for some 40+ years now and I go as far as to call him an expert on what he is doing. I was helping him out in my holidays since I was at least 14 years old, so I basically know what to do around him and how to help - but in the few months we worked that close together I could take away a lot more than just money to spend on new computer stuff.
He always showed me how to place the materials in an efficient way, so one doesn't have to walk back and get more materials or have just a few steps less to walk.
At this point you probably ask yourself "where is this going and what does it have to do with programming?" and the answer is "everything".
In that time with my father I learned how to work efficiently and how to prepare my work (materials), so I can do my job without too many iterations and thus get better every time I do something. Everything you do is potentially improvable! Give it some thought: When you're cleaning the house, how do you place your working materials and in what order do you clean? Do you have to walk back and get something? Can you work seamlessly? Asking these questions and REALLY looking at what you're doing is the key. See if you can do the same work with less effort, less walking, less material or just "more efficiently".

Now back to the all-technical stuff :-)

When developing some new software, it's always the same:

 * Get a development environment
 * Provide some "common ground" for starting (framework install e.g.)
 * Do the usual database layout, controllers, views, templates
 * Create some fancy stuff, the customer wants
 * do some testing
 * create some kind of deployment
 * ...

Do you find some similarities to your projects?
Ok, let's take a closer look.

## Improving workflows

Every time you start a new project, you have to start somewhere. Did a sentence with "again" in it cross your mind? That might be the starting point for your first improvement. **A clear indicator for something that needs improvement is repetition**. If you do something several times the same way with slight variations, it's likely to be automatable.

* You need a Linux environment? Use e.g. vagrant or docker and VM images together with a provisioning tool like ansible
* New project, clone a framework, add some default tools like node, gulp, PHP classes? Use a skeleton project.
* Everytime you checkout that project you need to add configs to apache/nginx, restart services, ... use a task runner like [robo](http://robo.li)
* ...

I think you get the point there :-) Automation is your friend and there are many tools you can use to make your life better.

## Improving work

When I started coding I read a rule somewhere, that got stuck in my mind: "Everytime you edit/look at a piece of code, leave it better than before.". But what does that mean?
Most developers have some (or many) "old" systems to maintain. Meaning there's a legacy application with "bad code" and a lot of technical debt. No one likes these systems.
So, what you want to do is just "get the hell out of here" and leave it be. No?
The right answer should be: Make things better. Even if it's just tiny things you change, like moving to PSR-2, correct indentation, fix variable names, add inline comments, ... these things are likely to help the next developer who looks at this code. You can even go further, build a UnitTest for the class/module to have a specification for its behavior and then refactor the whole thing with the security of not breaking the application.
But these things take courage, so **be courageous**! Intergrate that behavior into your work and you will see that there is steady improvement in your codebase and also your feelings towards your code will improve. And don't be mad at the people (or yourself) who wrote that special piece of shitty code. Always remember: Most likely somone with a lot of skill and best intentions successfully solved a problem with his/her best tools available at that specific point in time. This makes it a lot easier. At least somewhat easier. If it's not complete nonsense code... 
Most developers say "I don't have enough time to do this" or "No one pays for this": This is correct - unless you integrate a small amount of refactoring cost into your estimations :-) I call this a "technical debt tax" and it is not explicitly in my estimations. It's always a few percent on top of the estimate and it will help a lot, over time. 

## Improve yourself

Nothing much to say, except: If you want to make your work and also your life better, always improve yourself.
Read about new technologies, try new things and find new ways of doing things. Even if it does not work it will help your understanding of how things work. So, never stop learning.

## In the end...

I could go on for much longer on how to improve, but in the end it boils down to a few guidelines and a lot of hard work to change your mindset:

 * Think about your problem domain and understand all aspects before you start to code
 * Always look for repetitive tasks and automate where you can (and it makes sense)
 * Improve what you find whenever you have to work on existing code - even small improvements help over time
 * Be lazy. Because a lazy person tends to be an efficient problem solver...
 * **Never** stop learning